#!/bin/sh

# fail fast
set -e

BASE=$(dirname "$(readlink -f "$0")")

# generate rules

j2 -e env -o "$BASE"/rules.txt "$BASE/"rules.j2 "$BASE"/rules.yml
