#!/bin/sh

# fail fast
set -e

BASE=$(dirname "$(readlink -f "$0")")

# generate the readmes
for platform in wiiu switch; do
  PLATFORM="$platform" j2 \
    -e env \
    -o "$BASE"/readme-"$platform".txt \
    "$BASE/"readme.j2 \
    "$BASE/"readme.yml
done
