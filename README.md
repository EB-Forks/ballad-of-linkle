# Ballad of Linkle

Dialogue fixes to make Breath of the Wild refer to Link (male) as Linkle (female) instead.

## Download

This repository is set up to build after every change. The most up-to-date Wii U and Switch files
can be downloaded by clicking the appropriate link below.

- [Switch][switch download]
- [Wii U][wiiu download]

[switch download]: https://gitlab.com/jkcclemens/ballad-of-linkle/-/jobs/artifacts/dev/raw/switch.7z?job=release
[wiiu download]: https://gitlab.com/jkcclemens/ballad-of-linkle/-/jobs/artifacts/dev/raw/wiiu.7z?job=release
