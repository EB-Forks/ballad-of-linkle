#!/bin/bash

set -e
shopt -s globstar

prepend() {
  local to_prepend=$1
  while read -r line; do
    echo "$to_prepend $line"
  done
}

set_up_temp_dir() {
  local tempdir
  tempdir=$(mktemp -p /tmp -d bol-pack.XXXXXXXX)

  echo "$tempdir"
}

main() {
  local platforms
  if [ "$*" = "" ]; then
    platforms=("wiiu" "switch")
  else
    for arg in "$@"; do
      case "$arg" in
        "wiiu") platforms+=("$arg") ;;
        "switch") platforms+=("$arg") ;;
        *)
          echo "invalid platform $arg (must specify one or more of switch or wiiu)"
          exit 1
          ;;
      esac
    done
  fi

  # create temporary directory for working
  local tempdir
  tempdir=$(set_up_temp_dir)
  trap "rm -rf -- $tempdir" EXIT

  # copy msyts and resources to working dir
  cp -r msyt "$tempdir/msyt"
  cp -r resources "$tempdir/resources"

  # clean up any msbts
  rm -f "$tempdir"/**/*.msbt

  local original_dir
  original_dir=$(pwd)

  # create msbts from msyts
  echo "Creating MSBTs..."
  for platform in "${platforms[@]}"; do
    msyt create --directory --no-backup --platform "$platform" --output "$tempdir/$platform" "$tempdir/msyt"
  done

  # remove the msyts
  rm -- "$tempdir"/**/*.msyt

  # move to working dir
  cd "$tempdir"

  # make output dirs
  for platform in "${platforms[@]}"; do
    mkdir -p output/"$platform"/System/Resource output/"$platform"/Pack
  done

  # pack the msbts
  for platform in "${platforms[@]}"; do
    echo "Packing $platform..."
    pack_msbts "$platform"
  done

  # go back
  cd "$original_dir"

  # move outputs
  mv "$tempdir/output" ./

  # replace updated dirs
  rm -rf resources
  mv "$tempdir"/resources ./

  echo "Done."
  echo "See $original_dir/output"
}

pack_msbts() {
  local sub=$1
  local args=""
  [ "$sub" = "wiiu" ] && args="-b"

  sarc c $args "$sub" Msg_USen.product.ssarc 2>&1 | prepend "  [sarc]"
  rstbtool $args resources/"$sub"/ResourceSizeTable.product.srsizetable set Message/Msg_USen.product.sarc Msg_USen.product.ssarc | prepend "  [rstb]"
  rstbtool $args resources/"$sub"/ResourceSizeTable.product.srsizetable set Message/Msg_EUen.product.sarc Msg_USen.product.ssarc | prepend "  [rstb]"
  mkdir -p pack/Bootup_{EU,US}en/Message
  cp Msg_USen.product.ssarc pack/Bootup_EUen/Message/Msg_EUen.product.ssarc
  mv Msg_USen.product.ssarc pack/Bootup_USen/Message
  cd pack
  sarc c $args Bootup_EUen Bootup_EUen.pack 2>&1 | prepend "  [sarc]"
  sarc c $args Bootup_USen Bootup_USen.pack 2>&1 | prepend "  [sarc]"
  mv ./*.pack ../output/"$sub"/Pack/
  cd ..
  rm -rf pack
  cp resources/"$sub"/ResourceSizeTable.product.srsizetable output/"$sub"/System/Resource/
}

main "$@"
